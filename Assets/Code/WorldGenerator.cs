using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Tilemaps;

public class WorldGenerator : MonoBehaviour
{
    [SerializeField] private GenerationCenter generationCenter;

    [SerializeField] private int squareSize = 100;

    [SerializeField] private Tilemap tileMap;

    [SerializeField] private TileBase dirt;
    [SerializeField] private TileBase sky;

    [SerializeField] private TileBase root;

    private HashSet<Vector2Int> destroyedTiles = new HashSet<Vector2Int>();

    private RectInt currentViewBounds
    {
        get
        {
            RectInt bounds = new RectInt(
                generationCenter.position.Value.x - (squareSize / 2),
                generationCenter.position.Value.y - (squareSize / 2),
                squareSize,
                squareSize);
            return bounds;
        }
    }
    
    private List<Root> roots = new List<Root>();

    void Start()
    {
        CreateRoots();
        Generate();
        generationCenter.position.onChange += (oldValue, newValue) =>
        {
            Generate();
        };
    }

    private void CreateRoots()
    {
        roots.Add(new Root());
        roots.Add(new Root());
        roots.Add(new Root());
        roots.Add(new Root());
        roots.Add(new Root());
        roots.Add(new Root());
        roots.Add(new Root());
        roots.Add(new Root());
        roots.Add(new Root());
        roots.Add(new Root());
    }

    private void Generate()
    {
        CullRoots();
        int startX = generationCenter.position.Value.x - (squareSize / 2);
        int starty = generationCenter.position.Value.y - (squareSize / 2);
        for (int x = startX; x < startX + squareSize; x++)
        {
            for (int y = starty; y < starty + squareSize; y++)
            {
                Vector2Int cellPos = new Vector2Int(x, y);
                
                if (HasTile(cellPos))
                    continue;
                
                var cell = Shade(cellPos);
                tileMap.SetTile((Vector3Int)cellPos, cell.groundTile);
                tileMap.SetTile((Vector3Int)cellPos + Vector3Int.forward, cell.rootTile);
            }
        }
        ClearBorders();
    }

    private void CullRoots()
    {
        foreach (var root in roots)
        {
            if (currentViewBounds.Overlaps(root.bounds))
                root.Load();
            else
                root.Unload();
        }
    }

    private void ClearBorders()
    {
        int halfSquareSize = squareSize / 2;
        Vector3Int position = (Vector3Int)generationCenter.position.Value;
        
        //lhs
        int lhsx = position.x - halfSquareSize - 1;
        for (int y = position.y - halfSquareSize - 1; y < position.y + squareSize + 2; y++)
        {
            tileMap.SetTile(new Vector3Int(lhsx, y), null);
        }
        
        //top
        int topy = position.y + halfSquareSize + 1;
        for (int x = position.x - halfSquareSize - 1; x <  position.x +squareSize + 2; x++)
        {
            tileMap.SetTile(new Vector3Int(x,topy ), null);
        }
        
        //bottom
        int bottomy = position.y - halfSquareSize - 1;
        for (int x = position.x - halfSquareSize - 1; x <  position.x +squareSize + 2; x++)
        {
            tileMap.SetTile(new Vector3Int(x,bottomy ), null);
        }
        
        //lhs
        int rhsx = position.x + halfSquareSize + 1;
        for (int y = position.y - halfSquareSize - 1; y < position.y + squareSize + 2; y++)
        {
            tileMap.SetTile(new Vector3Int(rhsx, y), null);
        }
    }

    private ShadedCell Shade(Vector2Int cellPos)
    {
        
        
        bool underHorizon = cellPos.y < Mathf.Sin(cellPos.x/(Mathf.Sin(cellPos.x / 5f) + 7)) * Mathf.Sin(cellPos.x/20f) * 5f;

        ShadedCell cell = new ShadedCell();

        if (destroyedTiles.Contains(cellPos))
        {
            cell.groundTile = sky;
            cell.rootTile = null;
            return cell;
        }

        if (underHorizon)
        {
            cell.groundTile = dirt;
        }
        else
            cell.groundTile = sky;

        var hasRoot = roots.Select(x => x.Sample(cellPos)).Any(x => x);
        if (hasRoot)
            cell.rootTile = root;
        
        return cell;
    }

    public void DestroyTile(Vector2Int position)
    {
        tileMap.SetTile((Vector3Int)position, sky);
        tileMap.SetTile((Vector3Int)position + Vector3Int.forward, null);
        destroyedTiles.Add(position);
    }

    public bool HasTile(Vector2Int position)
    {
        var tile = tileMap.GetTile(new Vector3Int(position.x, position.y, 0));

        if (tile != sky && tile != null)
            return true;
        return false;
    }
    
    private struct ShadedCell
    {
        [CanBeNull] public TileBase groundTile;
        [CanBeNull] public TileBase rootTile;
    }
}
