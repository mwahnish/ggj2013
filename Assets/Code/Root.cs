using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;

public class Root 
{
    public RectInt bounds { get; private set; }
    private int seed = 0;

    private List<Segment> positions = new List<Segment>();
    private Dictionary<Vector2Int,Segment> positionSet = new Dictionary<Vector2Int, Segment>();

    private int segmentCount = 0;
    
    public bool loaded { get; private set; }
    
    public Root()
    {
        seed = Random.Range(0, 10000);
        int size = Random.Range(20, 300);
        int x = Random.Range(-1000, 1000);
        int y = Random.Range(10 - (size/2), -200);
        bounds = new RectInt(new Vector2Int(x - (size/2), y- (size/2)), new Vector2Int(size, size));
        Debug.Log($"roots bounds: {bounds.center}" );
        segmentCount = (size * size);
    }

    public void Load()
    {
        if (loaded)
            return;
        Debug.Log("loading");
        
        Random.InitState(seed);
        var center = new Segment(new Vector2Int((int)bounds.center.x, (int)bounds.center.y));
        positions.Add(center);
        positionSet.Add(center.position, center);
        
        for (int i = 0; i < segmentCount; i++)
        {
            AddSegment();
        }

        loaded = true;
    }

    private void AddSegment()
    {
        Vector2 rootSystemCenter = bounds.center;
        
        // pick a random point
        int randomIndex = Random.Range(0, positions.Count - 1);
        
        // building the possible positions list
        List<Vector2Int> surroundingPositions = new List<Vector2Int>();
        foreach (var position in positions[randomIndex].position.IterateSurroundingPositions())
        {
            if (positionSet.ContainsKey(position))
                continue; // skipping if there's already a root in this position

            if (!bounds.Contains(position))
                continue;

            // skipping elements with too many neighbors to prevent things from getting too dense
            if (position.IterateSurroundingPositions().Count(x => positionSet.ContainsKey(x)) > 2)
                continue;
            

            // Weighing options that are farther than the target point from the center, so it tends to grow outwards
            if (Vector2.Distance(rootSystemCenter, position) >
                Vector2.Distance(rootSystemCenter, positions[randomIndex].position))
            {
                surroundingPositions.Add(position);
            }

        }

        if (surroundingPositions.Count == 0)
            return;
        
        
        // picking one
        int finalSelectionIndex = Random.Range(0, surroundingPositions.Count-1);
        Vector2Int selection = surroundingPositions[finalSelectionIndex];

        Segment newSegment = new Segment(selection);
        positions[randomIndex].children.Add(newSegment);
        
        positions.Add(newSegment);
        positionSet.Add(selection, newSegment);

    }
    
    public void Unload()
    {
        if (!loaded)
            return;
        Debug.Log("Unloaded");
        positions.Clear();
        positionSet.Clear();
        loaded = false;
    }

    public bool Sample(Vector2Int position)
    {
        if (! loaded)
            return false;
        var result = positionSet.ContainsKey(position);
        return result;
    }

    private class Segment
    {
        public Vector2Int position;
        public List<Segment> children = new List<Segment>();

        public Segment(Vector2Int position)
        {
            this.position = position;
        }
    }

}
