using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeableType<InnerType>
{
    private InnerType _value;

    public InnerType Value
    {
        get => _value;
        set
        {
            
            //Change detection;
            bool nullDifferent = (_value == null && value != null) || (_value != null && value == null);
            bool changed = nullDifferent || !_value.Equals(value);

            InnerType oldValue = _value;
            _value = value;
            if (changed)
            {
                onChange?.Invoke(oldValue, value);
            }
        }
    }
    
    public delegate void OnChangeDelegate(InnerType oldValue, InnerType newValue);

    public OnChangeDelegate onChange;
}
