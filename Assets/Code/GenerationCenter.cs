using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerationCenter : MonoBehaviour
{
    public ChangeableType<Vector2Int> position { get; private set; } = new ChangeableType<Vector2Int>();
    // Start is called before the first frame update
    void Start()
    {
        position.onChange += (original, newPosition) =>
        {
            Debug.Log($"New Position: {position.Value}");
        };
    }

    // Update is called once per frame
    void Update()
    {
        position.Value =
            new Vector2Int(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y));
        
    }
}
