using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vector2 = System.Numerics.Vector2;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private Rigidbody2D rb;

    [SerializeField] private float jumpForce;

    [SerializeField] private float moveForce;

    [SerializeField] private float maxVelocity = 1000f;

    [SerializeField] private Transform toolRotator;

    private float toolRotation = 0;
    private Vector3 toolRotationVirtualStick = Vector3.zero;
    private Vector3 lastFrameMousePosition = Vector3.zero;
    [SerializeField] private float virtualStickDeadzone = 1f;

    [SerializeField] private List<Item> items = new List<Item>();

    private ChangeableType<Item> currentItem = new ChangeableType<Item>();

    [SerializeField] private SpriteRenderer playerItemRenderer;

    [SerializeField] private WorldGenerator world;

    public RectInt playerRect
    {
        get
        {
            Vector3 blUnrounded = transform.position + new Vector3(-.5f, -1f, 0);
            Vector2Int bl = new Vector2Int(Mathf.RoundToInt(blUnrounded.x), Mathf.RoundToInt(blUnrounded.y));
            return new RectInt(bl, new Vector2Int(1, 2));
            
        }
    }

    public bool grounded
    {
        get { return RaycastToGround(); }
    }
    
    // Start is called before the first frame update
    void Start()
    {
        lastFrameMousePosition = Input.mousePosition;

        for (int index = 0; index < items.Count; index++)
            items[index] = ScriptableObject.Instantiate(items[index]);
        
        currentItem.onChange += OnItemChange;
        currentItem.Value = items[0];
        QualitySettings.vSyncCount = 0;  // VSync must be disabled
        Application.targetFrameRate = 45;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (grounded && Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(new UnityEngine.Vector2(0,jumpForce));
        }

        if (Input.GetKey(KeyCode.A))
        {
            rb.AddForce(( new UnityEngine.Vector2(-1,0) * moveForce));
        }
        if (Input.GetKey(KeyCode.D))
        {
            rb.AddForce(( new UnityEngine.Vector2(1,0) * moveForce));
        }

        //calculating tool angle
        toolRotationVirtualStick += (Input.mousePosition - lastFrameMousePosition);
        //clamping virtual stick
        if (toolRotationVirtualStick.magnitude > virtualStickDeadzone)
            toolRotationVirtualStick = toolRotationVirtualStick.normalized * virtualStickDeadzone;
        // Applying data
        toolRotation = Vector3.SignedAngle(Vector3.right, toolRotationVirtualStick, Vector3.forward);
        toolRotator.rotation = Quaternion.AngleAxis(toolRotation, Vector3.forward);
        // storing rotation data for next frame
        lastFrameMousePosition = Input.mousePosition;

        if (rb.velocity.magnitude > maxVelocity)
            rb.velocity = rb.velocity.normalized * maxVelocity;
        
        // Item selection
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            currentItem.Value = items[1];
        }

        if (Input.GetMouseButton(0) && !currentItem.Value.inUse)
            StartCoroutine(currentItem.Value.DoAction(this, toolRotation, world));

        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }

    private void OnItemChange(Item oldItem, Item newItem)
    {
        playerItemRenderer.sprite = newItem.sceneSprite;
    }

    private bool RaycastToGround()
    {
        RaycastHit2D results = Physics2D.Raycast(
            this.transform.position,
            new UnityEngine.Vector2(0, -1), 1.5f, LayerMask.GetMask("Ground"));

        return results.collider != null;

    }

    private void OnDrawGizmos()
    {
        foreach (var position in playerRect.IterateSurroundingPositions())
        {
            
            Gizmos.DrawSphere(new Vector3(position.x, position.y, 0),0.1f);
        }
        
    }
}
