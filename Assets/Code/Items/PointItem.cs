using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Items/Pickaxe")]
public class PointItem : Item
{
    [SerializeField] private float range = 1.5f;
    protected override IEnumerator Use(PlayerController player, float angle, WorldGenerator world)
    {

        float closestAngleDistance = 90;
        Vector2Int closestPosition = new Vector2Int();
        bool foundTile = false;
        List<Vector2Int> tilesToCheck = new List<Vector2Int>();
        tilesToCheck.AddRange(player.playerRect.IterateSurroundingPositions().Where(x=>world.HasTile(x)));
        

        foreach (var position in tilesToCheck)
        {
            float currentPositionAngle =
                Vector3.SignedAngle(Vector3.right, position - player.playerRect.center, Vector3.forward);

            float angleDistance = Mathf.Abs(angle - currentPositionAngle);

            if (angleDistance < closestAngleDistance)
            {
                closestAngleDistance = angleDistance;
                closestPosition = position;
                foundTile = true;
            }
        }


        if (foundTile)
        {
            Debug.Log($"Destroying: {closestPosition}");
            world.DestroyTile(new Vector2Int((int)closestPosition.x, (int)closestPosition.y));
            yield return null;
        }
    }
}
