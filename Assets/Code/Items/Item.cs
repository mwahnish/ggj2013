using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;


public abstract class Item : ScriptableObject
{
    [SerializeField]
    public Sprite sceneSprite;

    [SerializeField] public Sprite uiSprite;

    [SerializeField] public float activationTimePeriod = 1f;

    private bool _inUse = false;
    public bool inUse
    {
        get => _inUse;
        protected set => _inUse = value;
    }

    public IEnumerator DoAction(PlayerController player, float angle, WorldGenerator world)
    {
        inUse = true;
        yield return Use(player, angle, world);
        yield return new WaitForSeconds(activationTimePeriod);
        inUse = false;
    }

    protected abstract IEnumerator Use(PlayerController player, float angle, WorldGenerator world);
    
    
}
