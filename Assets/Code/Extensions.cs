using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
    public static IEnumerable<Vector2Int> IterateSurroundingPositions(this Vector2Int self)
    {
        yield return new Vector2Int(-1, 0) + self;//l
        yield return new Vector2Int(-1, 1) + self;//tl
        yield return new Vector2Int(0, 1) + self;//t
        yield return new Vector2Int(1, 1) + self;//tr
        yield return new Vector2Int(1, 0) + self;//r
        yield return new Vector2Int(1, -1) + self;//br
        yield return new Vector2Int(0, -1) + self;//b
        yield return new Vector2Int(-1, -1) + self;//bl
    }

    public static IEnumerable<Vector2Int> IterateSurroundingPositions(this RectInt self)
    {
        Vector2Int blPosition = new Vector2Int(self.xMin, self.yMin);
        
        
        //lhs
        foreach (var element in IterateVerticalLine(blPosition + new Vector2Int(-1, -1), self.height + 2))
        {
            yield return element;
        }
        
        //rhs
        foreach (var element in IterateVerticalLine(blPosition + new Vector2Int(self.width , -1), self.height + 2))
        {
            yield return element;
        }
        
        //top
        foreach (var element in IterateHorizontalLine(blPosition + new Vector2Int(0, self.height ), self.width))
        {
            yield return element;
        }
        
        //bottom
        foreach (var element in IterateHorizontalLine(blPosition + new Vector2Int(0, -1), self.width))
        {
            yield return element;
        }

        
       
    }

    private static IEnumerable<Vector2Int> IterateVerticalLine(Vector2Int start, int count)
    {
        Vector2Int currentPosition = start;
        for (int i = 0; i < count; i++)
        {
            yield return currentPosition;
            currentPosition += new Vector2Int(0, 1);
        }
    }
    
    private static IEnumerable<Vector2Int> IterateHorizontalLine(Vector2Int start, int count)
    {
        Vector2Int currentPosition = start;
        for (int i = 0; i < count; i++)
        {
            yield return currentPosition;
            currentPosition += new Vector2Int(1, 0);
        }
    }
}
